require "socket"

def handle_client(client)
	name = "unidentified"
	loop do
		operation = client.read_byte
		case operation
		when 1
			tmp_name = client.gets
			name = tmp_name
			puts "#{name} connected"
			client.write_byte(0)
		when 2
			msg = client.gets
			puts "#{name}: #{msg}"
			client.write_byte(0)
		when 3
			puts "#{name} disconnected"
			client.write_byte(0)
			break
		when nil
			puts "#{name} hard disconnected"
			break
		else
			client.write_byte(1)
		end
	end
end

server = UNIXServer.new("/tmp/unixchat.sock")
Signal::INT.trap do
	puts "Closing server..."
	server.close
	exit 0
end
Signal::TERM.trap do
	puts "Closing server..."
	server.close
	exit 0
end
while client = server.accept?
	spawn handle_client(client)
end
puts "Closing server..."
server.close

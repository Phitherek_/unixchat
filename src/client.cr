require "socket"

begin
  sock = UNIXSocket.new("/tmp/unixchat.sock")
  loop do
          print "Name: "
          name = STDIN.gets.as(String).strip
          sock.write_byte(1)
          sock.puts name
          code = sock.read_byte
          if code == 0
                  break
          elsif code == nil
                  puts "Server hard disconnected!"
                  exit 1
          else
                  puts "Error!"
          end
  end
  loop do
          print "Message or command (/close to close): "
          str = STDIN.gets.as(String).strip
          if str == "/close"
                  sock.write_byte(3)
                  code = sock.read_byte
                  if code == 0
                          break
                  elsif code == nil
                          puts "Server hard disconnected!"
                          exit 1
                  else
                          puts "Error!"
                  end
          else
                  sock.write_byte(2)
                  sock.puts str
                  code = sock.read_byte
                  if code == 0
                          puts "Message sent!"
                  elsif code == nil
                          puts "Server hard disconnected!"
                          exit 1
                  else
                          puts "Error!"
                  end
          end
  end
  sock.close
rescue e : Socket::ConnectError
  puts "Could not connect to server: #{e.message}"
end

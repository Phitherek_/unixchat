# unixchat

A simple mock chat server using UNIX Sockets

## Installation

`shards build`

## Usage

`./bin/server` - to run the server
`./bin/client` - to run the client

## Development

1. Clone the repo
2. Run `shards`
3. Add code
4. Run `./bin/ameba` and ensure that it's green
5. Run `shards build` and check for compiler errors
6. If everything is good you can proceed with commiting changes

## Contributing

1. Fork it (<https://gitlab.com/Phitherek_/unixchat/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Follow instructions from Development
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to the branch (`git push origin my-new-feature`)
6. Create a new Merge Request

## Contributors

- [Phitherek_](https://gitlab.com/Phitherek_) - creator and maintainer
